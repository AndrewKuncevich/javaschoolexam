package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;//
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author Andrew
 */
public class PyramidBuilder {

    public static int height = 0;
    public static int weight = 0;
    public static boolean markEven;
    public static Stack<Integer> stack;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line
     * and maximum at the bottom, from left to right). All vacant positions in
     * the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be
     * build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException, NullPointerException {
        stack = new Stack<>(); //= (Stack<Integer>) inputNumbers;
        initTest(inputNumbers.size());

        markEven = inputNumbers.size() % 2 == 0;
        int s = inputNumbers.size();//
        sortList(inputNumbers);

        for (int i = 0; i < inputNumbers.size(); i++) {
            stack.push(inputNumbers.get(i));
        }

        height = getHeight(inputNumbers);
        weight = getWeight(height);

        int[][] b = new int[height][1 + 2 * (height - 1)];
        int[] buffer = null;

        if (markEven) {
        }

        writefirstline(inputNumbers, b);
        for (int i = 2; i <= height; i++) {
            writeNline(inputNumbers, b, i);
        }

        for (int i = 0; i < b.length; i++) {
            System.out.println(Arrays.toString(b[i]));
        }
        return b;
    }

    public static void sortList(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        try {
            List list = inputNumbers;
            Collections.sort(list, new Comparator<Integer>() {
                public int compare(Integer o1, Integer o2) {
                    return o2.compareTo(o1);
                }
            });
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
    }

    public static int getHeight(List<Integer> inputNumbers) {
        int height = 0;
        int s = inputNumbers.size();

        for (int i = 1; i < s; i++) {
            if (s == 0) {
                break;
            }

            for (int j = 0; j < height; j++) {
                if (s == 0) {
                    break;
                }
                s--;
            }
            height++;
        }
        System.out.println("Height" + height);
        return height;
    }

    /**
     * Write first line of matrix with first value of array
     */
    public static void writefirstline(List<Integer> inputNumbers, int[][] b) {
        b[0][weight / 2] = (int) stack.pop();

    }

    /**
     * Write N line of matrix. N!=1;
     */
    public static void writeNline(List<Integer> inputNumbers, int[][] b, int n) {
        int weight = getWeight(height);
        int k = 0;
        if (n % 2 == 1) {
            k = 1;
        }
        int z = 0;
        for (int i = 1; i <= weight; i = i + 2) {

            b[n - 1][(weight / 2) + i - n] = (int) stack.pop();
            z++;
            if (z == n) {
                break;
            }
        }
    }

    public static int getWeight(int n) {
        int k = (1 + 2 * (n - 1));
        System.out.println("Weight= " + k);
        return k;
    }

    public static void initTest(int n) {
        int k = n;
        for (int i = 1; i <= n; i++) {
            k = k - i;
            if (k == 0) {
                break;
            }
            if (k < 0) {
                throw new CannotBuildPyramidException();
            }
        }

    }
}
