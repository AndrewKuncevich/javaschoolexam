package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (initTest(x, y)) {
            return false;
        }
        if (x.size() == 0) {
            return true;
        }
        if (x.size() == 0 && y.size() == 0) {
            return true;
        }

        boolean mark = false;
        int k = 0;
        int z = 0;
        boolean marko1 = false;
        for (int i = 0; i < x.size(); i++) {//i=1
            Object o1 = x.get(i);
            for (int j = k; j < y.size(); j++) {
                Object o2 = y.get(j);
                if (o1 == o2) {
                    marko1 = true;
                    k = j + 1;
                    z = j + 1;
                    if (o1 == (x.get(x.size() - 1))) {
                        return true;
                    }
                    break;
                }
                marko1 = false;
            }
            if (!marko1) {
                return false;
            }
        }
        return mark;
    }

    public static boolean initTest(List x, List y) {
        try {
            if (x.isEmpty() && y.isEmpty()) {
                return false;
            }
            if (x.size() > y.size()) {
                return true;
            }
            if (y.isEmpty()) {
                return true;
            }
            if (x.isEmpty()) {
                return false;
            }

            if (x.get(0) == null | y.get(0) == null) {
                return true;
            }
        } catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }

        return false;
    }
}
