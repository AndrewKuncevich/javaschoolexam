package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;

public class Calculator {

    public static String temp1;         //use by solveStringMD
    public static String temp2;         // and solveScope methods
    public static int ans;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as
     * decimal mark, parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement
     * is invalid
     */
    public String evaluate(String statement) {
        if ("err".equals(testStatement(statement))) {
            return null;
        }

        if (findscope(statement)) {
            return solveStringPM(solveStringMD(solveScope(statement)));
        }
        return solveStringPM(solveStringMD(statement));
    }

    /**
     * solveStringPM solve arifmetic statemen such x1+x2+...xn with operation
     * '+' and '-'. solveStringPM is a basic metod. Complex statement solve by a
     * sequential transformation to x1+x2+...xn entry.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as
     * decimal mark, parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string result of expression x1+x2+...xn.
     */
    public String solveStringPM(String statement) {
        if (statement == null) {
            return null;
        }
        String first = getfirstnumber(statement);

        float ans1 = Float.valueOf(first);
        statement = statement.substring(first.length(), statement.length());
        int k = first.length();
        String second = "";
        while (second != null) {
            second = getfirstnumber(statement);
            if (second == null) {
                break;
            } else {
                k = second.length() + 1;
            }
            ans1 = ans1 + Float.valueOf(second);

            if (k > statement.length()) {
                break;
            }
            statement = statement.substring(k, statement.length());
        }

        DecimalFormat df = new DecimalFormat("##.####");
        String sg = df.format(ans1);
        String beuty = sg.replace(',', '.');
        return beuty;
    }

    /**
     * solveScope solve expression in scope of statement.
     *
     *
     * @param statement mathematical statement containing digits, '.' (dot) as
     * decimal mark, parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string without of scope. * Example statement 10/(5-3)+8 return
     * 10/2+5
     */
    public String solveScope(String statement) {
        int indexsc1 = 0;
        int indexsc2 = 0;

        for (int i = 0; i < statement.length(); i++) {
            char charAt = statement.charAt(i);
            if (charAt == '(') {
                indexsc1 = i;
            }
            if (charAt == ')') {
                indexsc2 = i;
                break;
            }
        }
        if (indexsc1 == 0 && indexsc1 == 0) {
            return statement;
        }
        String scope = statement.substring(indexsc1 + 1, indexsc2);
        temp1 = statement.substring(0, indexsc1);
        temp2 = statement.substring(indexsc2 + 1, statement.length());

        return temp1 + solveStringPM(solveScope(scope)) + temp2;
    }

    public String getfirstnumber(String statement) {
        if (statement.length() == 0) {
            return null;
        }
        if (statement.charAt(0) == '+') {
            statement = statement.substring(1, statement.length());
        }
        String first = "";
        ans = -1;
        if (statement.charAt(0) != '-') {
            int k = statement.indexOf("+");
            int b = statement.indexOf("-");

            if (k < b && k > 0 && b > 0) {
                ans = k;
            }
            if (k > b && k > 0 && b > 0) {
                ans = b;
            }
            if (k < 0 && b > 0) {
                ans = b;
            }
            if (b < 0 && k > 0) {
                ans = k;
            }
            if (b == -1 && k == -1) {
                first = statement;
                return first;
            }
            if (ans == -1) {
                return null;
            }
            first = statement.substring(0, ans);

        } else {
            statement = statement.substring(1, statement.length());
            int k = statement.indexOf("+");
            int b = statement.indexOf("-");
            if (k < b && k > 0 && b > 0) {
                ans = k;
            }
            if (k > b && k > 0 && b > 0) {
                ans = b;
            }
            if (k < 0 && b > 0) {
                ans = b;
            }
            if (b < 0 && k > 0) {
                ans = k;
            }
            if (b == -1 && k == -1) {
                first = "-" + statement;
                return first;
            }
            if (ans == -1) {
                return null;
            }
            first = statement.substring(0, ans);
            first = "-" + first;
        }
        return first;
    }

    /**
     * testStatement validation statement.
     *
     * @return string "err" if statement is invalid.
     */
    public static String testStatement(String s) {

        if ("".equals(s)) {
            return "err";
        }
        if (s == null) {
            return "err";
        }
        int p = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                p++;
            }
            if (s.charAt(i) == ')') {
                p--;
            }

            if (s.charAt(i) == ',') {
                return "err";
            }
            if (s.charAt(i) == '+' | s.charAt(i) == '-' | s.charAt(i) == '*' | s.charAt(i) == '/' | s.charAt(i) == '.') {
                if (s.charAt(i + 1) == '+' | s.charAt(i + 1) == '-' | s.charAt(i + 1) == '*' | s.charAt(i + 1) == '/' | s.charAt(i + 1) == '.') {
                    return "err";
                }
            }
        }
        if (p != 0) {
            return "err";
        }
        return "";
    }

    /**
     * solveStringMD on the first step find some '*'(or '/') operation and
     * return statement without this operation. Work recursively while statemen
     * will take the form acceptable for solveStringPM
     *
     * @param statement mathematical statement containing digits, '.' (dot) as
     * decimal mark, parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string expression in form x1+x2+...xn. Example 1+3*4+10/5-5
     * return 1+12+2-5
     */
    private String solveStringMD(String statement) {
        if (statement == null) {
            return null;
        }

        int z = statement.indexOf("*");
        int x = statement.indexOf("/");
        ans = -1;
        if (z < x && z > 0 && x > 0) {
            ans = z;
        }
        if (z > x && z > 0 && x > 0) {
            ans = x;
        }
        if (z < 0 && x > 0) {
            ans = x;
        }
        if (x < 0 && z > 0) {
            ans = z;
        }
        if (x == -1 && z == -1) {
            return statement;
        }

        String s = Character.toString(statement.charAt(ans));
        int g = ans;

        String sa = getleftN(statement, g);
        String sb = getrightN(statement, g);
        float a = Float.valueOf(sa);
        float b = Float.valueOf(sb);
        temp1 = statement.substring(0, g - sa.length());
        temp2 = statement.substring(g + 1 + sb.length(), statement.length());
        float minians = 0;
        if ("*".equals(s)) {
            minians = a * b;
        }
        if ("/".equals(s)) {
            if (b == 0) {
                return null;
            }
            minians = a / (float) b;
        }
        return solveStringMD(temp1 + minians + temp2);
    }

    public String getleftN(String s, int index) {
        String leftside = s.substring(0, index);
        int z = leftside.lastIndexOf("+");
        int x = leftside.lastIndexOf("-");
        ans = -1;
        if (z < x && z > 0 && x > 0) {
            ans = x;
        }
        if (z > x && z > 0 && x > 0) {
            ans = z;
        }
        if (z < 0 && x > 0) {
            ans = x;
        }
        if (x < 0 && z > 0) {
            ans = z;
        }
        String left = leftside.substring(ans + 1, leftside.length());
        return left;

    }

    public String getrightN(String s, int index) {
        String right = "";
        String rightside = s.substring(index + 1, s.length());
        int k = 0;
        if ("-".equals(Character.toString(rightside.charAt(0)))) {
            k++;
            right = "-" + right;
        }
        for (int i = k; i < rightside.length(); i++) {
            if ("+".equals(Character.toString(rightside.charAt(i)))
                    | "-".equals(Character.toString(rightside.charAt(i)))
                    | "*".equals(Character.toString(rightside.charAt(i)))
                    | "/".equals(Character.toString(rightside.charAt(i)))) {
                break;
            }
            right = right + Character.toString(rightside.charAt(i));
        }
        return right;
    }

    public boolean findscope(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                return true;
            }
        }
        return false;
    }

}
